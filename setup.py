from setuptools import setup

setup(name='berp',
        version-'.01',
        description='Module for handling the batch effect in sequencing data.'
        url='http://github.com/thekuhninator/berp',
        author='Roman Kuhn',
        author_email='roman.kuhn1@gmail.com',
        license='MIT',
        packages=['berp'],
        zip_safe=False)
